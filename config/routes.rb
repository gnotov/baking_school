Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'

  devise_for :users
  devise_for :admins

  root "main#index"

  resources :courses,     only: [:index, :show]
  resource  :cart,        only: [:show]
  resources :order_items, only: [:create, :update, :destroy]

  get  '/cart/checkout', to: 'carts#checkout'
  post '/cart/checkout', to: 'carts#checkout', as: :checkout
  post '/cart/create_user', to: 'carts#create_user', as: :checkout_user_create
  get  '/cart/place_order', to: 'carts#place_order', as: :place_order

  namespace :education do
    resources :courses, only: [:show] do
      member do
        get 'show_lesson'
      end
    end
  end

  namespace :admin do
    root "main#index"

    resources :courses
    resources :course_modules
    resources :lessons
    resources :teachers
    resources :categories
    resources :users
    resources :articles
    resources :orders
    resources :videos
    resources :tasks
  end
end
