class CartsController < ApplicationController

  def show
    @order_items = current_order.order_items
  end

  def checkout
    if !current_user
      @user = User.new
    else
      @user = current_user
    end

    if params[:order_item]
      @order = current_order
      @order_item = @order.order_items.new(order_item_params)
      @order.order_status_id = 1
      if current_user
        @order.user_id = current_user.id
      else
        @order.user_id = 1
      end
      @order.save
      session[:order_id] = @order.id
    else
      @order = current_order
    end

    puts @order
  end

  def create_user
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        sign_in @user
        @order = current_order
        @order.user_id = @user.id
        @order.save
        format.html { redirect_to(checkout_path, notice: "User was successfully created.") }
      else
        redirect_to checkout_path, notice: "Please fill all fields."
      end
    end
  end

  def place_order
    @order = current_order
    @order.order_status_id = 2
    @order.save

    respond_to do |format|
      if @order.save
        #session.delete(:order_id)
        format.html { redirect_to(checkout_path, notice: "Order has been placed.") }
      else
        redirect_to checkout_path, notice: "Please fill all fields."
      end
    end
  end

  private

  def order_item_params
    params.require(:order_item).permit(:quantity, :order_id, :course_id, :unit_price, :quantity, :total_price)
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password)
  end
end
