class Admin::ArticlesController < AdminController
  before_action :set_article, only: [:show, :edit, :update, :destroy]

  def index
    @articles = Article.all.order("created_at DESC")
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @article = Article.new

    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to(admin_articles_url, notice: 'Article was successfully created.') }
      else
        @articles = Article.all
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @article.update_attributes(article_params)

        format.html { redirect_to(admin_articles_url, notice: 'Article was successfully updated.') }
      else
        @articles = Article.all
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @article.destroy

    respond_to do |format|
      format.html { redirect_to(admin_articles_url) }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    def article_params
      params.require(:article).permit(:title, :description, :active, :main_img, :tag_list, :slug)
    end

end
