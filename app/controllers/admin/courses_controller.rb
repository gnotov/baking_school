class Admin::CoursesController < AdminController
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  def index
    @courses = Course.all.order("created_at DESC")
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @course = Course.new

    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    @course = Course.new(course_params)

    respond_to do |format|
      if @course.save
        format.html { redirect_to(admin_courses_url, notice: 'Course was successfully created.') }
      else
        @courses = Course.all
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @course.update_attributes(course_params)

        format.html { redirect_to(admin_courses_url, notice: 'Course was successfully updated.') }
      else
        @courses = Course.all
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @course.destroy

    respond_to do |format|
      format.html { redirect_to(admin_courses_url) }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:name, :price, :active, :description, :cover_photo, :avatar, :category_id, teacher_ids: [])
    end

end
