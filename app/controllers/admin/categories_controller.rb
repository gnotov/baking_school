class Admin::CategoriesController < AdminController
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.all.order("created_at DESC")
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @category = Category.new

    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to(admin_categories_url, notice: 'Category was successfully created.') }
      else
        @categories = Category.all
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @category.update_attributes(category_params)

        format.html { redirect_to(admin_categories_url, notice: 'Category was successfully updated.') }
      else
        @categories = Category.all
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @category.destroy

    respond_to do |format|
      format.html { redirect_to(admin_categories_url) }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name)
    end

end
