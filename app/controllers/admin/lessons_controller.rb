class Admin::LessonsController < AdminController
  before_action :set_lesson, only: [:show, :edit, :update, :destroy]

  def index
    @lessons = Lesson.all.order("created_at DESC").includes(:course, :course_module)
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @lesson = Lesson.new

    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    @lesson = Lesson.new(lesson_params)

    respond_to do |format|
      if @lesson.save
        format.html { redirect_to(admin_lessons_url, notice: 'Lesson was successfully created.') }
      else
        @lessons = Lesson.all
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @lesson.update_attributes(lesson_params)

        format.html { redirect_to(admin_lessons_url, notice: 'Lesson was successfully updated.') }
      else
        @lessons = Lesson.all
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @lesson.destroy

    respond_to do |format|
      format.html { redirect_to(admin_lessons_url) }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_lesson
      @lesson = Lesson.find(params[:id])
    end

    def lesson_params
      params.require(:lesson).permit(:name, :description, :video_url, :order_number, :pdf, :timing, :skill_lvl, :course_id, :course_module_id)
    end

end
