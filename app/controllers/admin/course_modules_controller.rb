class Admin::CourseModulesController < AdminController
  before_action :set_course_module_module, only: [:show, :edit, :update, :destroy]

  def index
    @course_modules = CourseModule.all.order("created_at DESC")
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @course_module = CourseModule.new

    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    @course_module = CourseModule.new(course_module_params)

    respond_to do |format|
      if @course_module.save
        format.html { redirect_to(admin_course_modules_url, notice: 'Module was successfully created.') }
      else
        @course_modules = CourseModule.all
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @course_module.update_attributes(course_module_params)

        format.html { redirect_to(admin_course_modules_url, notice: 'Module was successfully updated.') }
      else
        @course_modules = CourseModule.all
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @course_module.destroy

    respond_to do |format|
      format.html { redirect_to(admin_course_modules_url) }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_course_module
      @course_module = CourseModule.find(params[:id])
    end

    def course_module_params
      params.require(:course_module).permit(:name, :description, :order_number, :active, :course_id, teacher_ids: [])
    end

end
