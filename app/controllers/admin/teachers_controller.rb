class Admin::TeachersController < AdminController
  before_action :set_teacher, only: [:show, :edit, :update, :destroy]

  def index
    @teachers = Teacher.all.order("created_at DESC")
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @teacher = Teacher.new

    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    @teacher = Teacher.new(teacher_params)

    respond_to do |format|
      if @teacher.save
        format.html { redirect_to(admin_teachers_url, notice: 'Teacher was successfully created.') }
      else
        @teachers = Teacher.all
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @teacher.update_attributes(teacher_params)

        format.html { redirect_to(admin_teachers_url, notice: 'Teacher was successfully updated.') }
      else
        @teachers = Teacher.all
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @teacher.destroy

    respond_to do |format|
      format.html { redirect_to(admin_teachers_url) }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_teacher
      @teacher = Teacher.find(params[:id])
    end

    def teacher_params
      params.require(:teacher).permit(:first_name, :last_name, :description, :website_url, :insta_url, :fb_url, :vk_url, :avatar)
    end

end
