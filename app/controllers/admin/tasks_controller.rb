class Admin::TasksController < AdminController
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  def index
    @tasks = Task.all.order("created_at DESC").includes(:lesson)
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @task = Task.new

    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    @task = Task.new(task_params)

    respond_to do |format|
      if @task.save
        format.html { redirect_to(admin_tasks_url, notice: 'Task was successfully created.') }
      else
        @tasks = Task.all
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @task.update_attributes(task_params)

        format.html { redirect_to(admin_tasks_url, notice: 'Task was successfully updated.') }
      else
        @tasks = Task.all
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @task.destroy

    respond_to do |format|
      format.html { redirect_to(admin_tasks_url) }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:name, :description, :active, :lesson_id)
    end

end
