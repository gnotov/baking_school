class Admin::OrdersController < AdminController
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  def index
    @orders = Order.all.order("created_at DESC").includes(:user, :order_status)
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @order = Order.new

    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save
        format.html { redirect_to(admin_orders_url, notice: 'Order was successfully created.') }
      else
        @orders = Order.all
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @order.update_attributes(order_params)

        format.html { redirect_to(admin_orders_url, notice: 'Order was successfully updated.') }
      else
        @orders = Order.all
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @order.destroy

    respond_to do |format|
      format.html { redirect_to(admin_orders_url) }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    def order_params
      params.require(:order).permit(:order_status_id, :user_id)
    end

end
