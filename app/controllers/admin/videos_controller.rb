class Admin::VideosController < AdminController
  before_action :set_video, only: [:show, :edit, :update, :destroy]

  def index
    @videos = Video.all.order("created_at DESC").includes(:lesson)
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def new
    @video = Video.new

    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  def create
    @video = Video.new(video_params)

    respond_to do |format|
      if @video.save
        format.html { redirect_to(admin_videos_url, notice: 'Video was successfully created.') }
      else
        @videos = Video.all
        format.html { render :action => "new" }
      end
    end
  end

  def update
    respond_to do |format|
      if @video.update_attributes(video_params)

        format.html { redirect_to(admin_videos_url, notice: 'Video was successfully updated.') }
      else
        @videos = Video.all
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @video.destroy

    respond_to do |format|
      format.html { redirect_to(admin_videos_url) }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = Video.find(params[:id])
    end

    def video_params
      params.require(:video).permit(:url, :embed_code, :price, :lesson_id)
    end

end
