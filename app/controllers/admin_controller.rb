class AdminController < ApplicationController
  protect_from_forgery with: :exception
  layout 'admin'
  before_action :authenticate_admin!

  def authenticate_admin!
    if current_admin.nil?
      redirect_to new_admin_session_path, alert: "Not authorized"
    end
  end
end
