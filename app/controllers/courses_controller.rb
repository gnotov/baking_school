class CoursesController < ApplicationController

  def index
    @courses = Course.all
    @order_item = current_order.order_items.new
  end

  def show
    @course = Course.find(params[:id])
    @order_item = current_order.order_items.new
  end

end
