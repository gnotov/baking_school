class Education::CoursesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_course, only: [:show]
  before_action :set_lesson, only: [:show_lesson]

  def show
    if Order.where(user_id: current_user.id).joins(:order_items).where(order_items: { course_id: @course.id }).exists?
      @first_lesson = @course.course_modules.first.lessons.first

      @first_lesson.tasks.each do |task|
        unless CookingTask.where(user_id: current_user.id, task_id: task.id).exists?
          @cooking_task = CookingTask.new(user_id: current_user.id, task_id: task.id)
          @cooking_task.save
        end
      end
    else
      redirect_to(courses_path, notice: "You are not prepared!")
    end
  end

  def show_lesson
    @course = @lesson.course
  end

  private

  def set_course
    @course = Course.find(params[:id])
  end

  def set_lesson
    @lesson = Lesson.find(params[:id])
  end
end
