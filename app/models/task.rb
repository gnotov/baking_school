class Task < ApplicationRecord
  belongs_to :lesson
  has_many :cooking_tasks
  has_many :users, through: :cooking_tasks
end
