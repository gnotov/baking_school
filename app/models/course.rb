class Course < ApplicationRecord
  has_many :course_modules
  has_many :lessons
  has_many :comments
  has_many :order_items

  has_and_belongs_to_many :teachers

  belongs_to :category

  has_attached_file :cover_photo, styles: { medium: "800x600>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :cover_photo, content_type: /\Aimage\/.*\z/

  has_attached_file :avatar, styles: { medium: "800x600>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
end
