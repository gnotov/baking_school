class Lesson < ApplicationRecord
  belongs_to :course
  belongs_to :course_module

  has_many :photos
  has_many :videos
  has_many :student_photos
  has_many :tasks

  accepts_nested_attributes_for :photos, allow_destroy: true

  has_attached_file :pdf
  validates_attachment :pdf, :content_type => { :content_type => %w(application/pdf) }
end
