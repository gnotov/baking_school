class Video < ApplicationRecord
  belongs_to :lesson
  
  has_many :order_items
end
