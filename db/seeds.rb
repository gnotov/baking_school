OrderStatus.delete_all
OrderStatus.create! id: 1, name: "In Progress"
OrderStatus.create! id: 2, name: "Placed"
OrderStatus.create! id: 3, name: "Completed"
OrderStatus.create! id: 4, name: "Cancelled"

User.delete_all
User.create! id: 1, first_name: "Nikita", last_name: "Gnotovs", email: "n.gnotov@gmail.com", password: "qwerty"
