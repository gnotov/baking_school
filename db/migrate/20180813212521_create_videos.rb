class CreateVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :videos do |t|
      t.string  :url,       default: ""
      t.decimal :price
      t.integer :lesson_id

      t.timestamps
    end

    add_index :videos, :lesson_id
  end
end
