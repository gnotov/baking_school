class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string  :name,        default: ""
      t.text    :description, default: ""
      t.boolean :active
      t.integer :lesson_id

      t.timestamps
    end

    add_index :tasks, :lesson_id
  end
end
