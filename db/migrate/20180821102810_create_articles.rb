class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string     :title      , default: ""
      t.text       :description, default: ""
      t.boolean    :active     , default: false
      t.attachment :main_img

      t.timestamps
    end

    add_index :articles, :title
  end
end
