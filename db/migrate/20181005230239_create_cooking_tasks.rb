class CreateCookingTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :cooking_tasks do |t|
      t.integer :user_id
      t.integer :task_id
      t.boolean :completed, default: false

      t.timestamps
    end

    add_index :cooking_tasks, :user_id
    add_index :cooking_tasks, :task_id
  end
end
