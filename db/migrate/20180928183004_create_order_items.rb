class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.integer    :course_id
      t.integer    :order_id
      t.decimal    :unit_price,  precision: 12, scale: 3
      t.integer    :quantity
      t.decimal    :total_price, precision: 12, scale: 3

      t.timestamps
    end

    add_index :order_items, :course_id
    add_index :order_items, :order_id
  end
end
