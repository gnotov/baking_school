class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.decimal    :total,        precision: 12, scale: 3
      t.integer    :order_status_id
      t.integer    :user_id

      t.timestamps
    end

    add_index :orders, :order_status_id
    add_index :orders, :user_id
  end
end
