class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string     :name,        default: ""
      t.decimal    :price
      t.boolean    :active
      t.text       :description, default: ""
      t.attachment :cover_photo
      t.attachment :avatar
      t.integer    :category_id

      t.timestamps
    end

    add_index :courses, :category_id
  end
end
