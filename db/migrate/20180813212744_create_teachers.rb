class CreateTeachers < ActiveRecord::Migration[5.2]
  def change
    create_table :teachers do |t|
      t.string     :first_name,  default: ""
      t.string     :last_name,   default: ""
      t.text       :description, default: ""
      t.string     :website_url, default: ""
      t.string     :insta_url,   default: ""
      t.string     :fb_url,      default: ""
      t.string     :vk_url,      default: ""
      t.attachment :avatar

      t.timestamps
    end
  end
end
