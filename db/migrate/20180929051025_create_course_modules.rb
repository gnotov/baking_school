class CreateCourseModules < ActiveRecord::Migration[5.2]
  def change
    create_table :course_modules do |t|
      t.string     :name
      t.text       :description
      t.boolean    :active
      t.integer    :order_number
      t.integer    :course_id

      t.timestamps
    end

    add_index :course_modules, :course_id
  end
end
