class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text    :comment_text, default: ""
      t.integer :user_id
      t.integer :lesson_id

      t.timestamps
    end

    add_index :comments, :user_id
    add_index :comments, :lesson_id
  end
end
