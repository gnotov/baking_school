class CreateLessons < ActiveRecord::Migration[5.2]
  def change
    create_table :lessons do |t|
      t.string     :name,        default: ""
      t.text       :description, default: ""
      t.string     :video_url,   default: ""
      t.integer    :order_number
      t.attachment :pdf
      t.string     :timing,      default: ""
      t.string     :skill_lvl,   default: ""
      t.integer    :course_id
      t.integer    :course_module_id

      t.timestamps
    end

    add_index :lessons, :course_id
    add_index :lessons, :name
  end
end
