class CreateStudentPhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :student_photos do |t|
      t.attachment :image
      t.integer    :lesson_id
      t.integer    :user_id

      t.timestamps
    end

    add_index :student_photos, :lesson_id
    add_index :student_photos, :user_id
  end
end
