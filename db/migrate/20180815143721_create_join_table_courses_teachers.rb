class CreateJoinTableCoursesTeachers < ActiveRecord::Migration[5.2]
  def change
    create_join_table :courses, :teachers do |t|
      t.index :course_id
      t.index :teacher_id
    end
  end
end
