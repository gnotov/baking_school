class CreatePhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :photos do |t|
      t.attachment :image
      t.integer    :lesson_id

      t.timestamps
    end

    add_index :photos, :lesson_id
  end
end
